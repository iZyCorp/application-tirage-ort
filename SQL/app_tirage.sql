-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : dim. 27 fév. 2022 à 13:35
-- Version du serveur : 5.7.36
-- Version de PHP : 7.4.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `app_tirage`
--

-- --------------------------------------------------------

--
-- Structure de la table `etudiant`
--

DROP TABLE IF EXISTS `etudiant`;
CREATE TABLE IF NOT EXISTS `etudiant` (
  `IdEtudiant` int(11) NOT NULL AUTO_INCREMENT,
  `NomEtudiant` varchar(50) DEFAULT NULL,
  `PrenomEtudiant` varchar(50) DEFAULT NULL,
  `IdSection` int(11) DEFAULT NULL,
  PRIMARY KEY (`IdEtudiant`),
  KEY `SECTION_FK` (`IdSection`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `etudiant`
--

INSERT INTO `etudiant` (`IdEtudiant`, `NomEtudiant`, `PrenomEtudiant`, `IdSection`) VALUES
(1, 'AKAY', 'Omer', 1),
(2, 'BOISSIE', 'Angel', 1),
(3, 'BONDOUX', 'Lenny', 1),
(4, 'BOUMALLASSA', 'Ayoub', 1),
(5, 'BROYER', 'Mathys', 1),
(6, 'BRUGERE', 'Augustin', 1),
(7, 'CROCHON', 'Sylvain', 1),
(8, 'DE ASCENCAO PEREIRA', 'Damien', 1),
(9, 'GARCIA DE LA PINERA', 'Victorio', 1),
(10, 'LAFI', 'Rayan', 1),
(11, 'LECONTE', 'Benjamin', 1),
(12, 'MEUNIER', 'Alexandre', 1),
(13, 'MHRIZER ', 'Mohamed-jessim', 1),
(14, 'NEEMA', 'Florian', 1),
(15, 'NEKKOUR', 'Ayoub', 1),
(16, 'PATOUILLARD', 'Virgile', 1),
(17, 'PRESLE', 'Luca', 1),
(18, 'SAPET', 'Arnaud', 1),
(19, 'SAVARY', 'Augustine', 1),
(20, 'ABDALLAH', 'Toufaha', 2),
(21, 'ALYACOUB', 'Gabriel', 2),
(22, 'BERNARD BRET', 'Chloë', 2),
(23, 'BEUROT', 'Mathis', 2),
(24, 'CORRE', 'Romain', 2),
(25, 'FERRIER', 'Victor', 2),
(26, 'GILLET', 'Thomas', 2),
(27, 'KEUGA FOTSI', 'Maella', 2),
(28, 'LADJILI', 'Khalil', 2),
(29, 'LEDJMI', 'Jessim', 2),
(30, 'MARION', 'Théodore Cléophas', 2),
(31, 'NEDJAH', 'Lucas', 2),
(32, 'PENELON', 'Hugo', 2),
(33, 'PERRICHON', 'Martin', 2),
(34, 'QUESNOT', 'Scotty', 2),
(35, 'RODRIGUEZ', 'Thomas', 2),
(36, 'SAMORA', 'Ulrich', 2),
(37, 'SILVESTRE', 'Axel', 2),
(38, 'VIERA NUNES', 'Ruben Filipe', 2),
(39, 'VONGKINGKEO', 'Kenzo', 2),
(40, 'LeChien', 'Walter', 3),
(41, 'UnAutreChien', 'Doge', 3),
(42, 'Chat', 'BongoCat', 3);

-- --------------------------------------------------------

--
-- Structure de la table `section`
--

DROP TABLE IF EXISTS `section`;
CREATE TABLE IF NOT EXISTS `section` (
  `IdSection` int(11) NOT NULL AUTO_INCREMENT,
  `Libelle` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`IdSection`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `section`
--

INSERT INTO `section` (`IdSection`, `Libelle`) VALUES
(1, '1SIO'),
(2, '2SIO'),
(3, '66SIO');

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `etudiant`
--
ALTER TABLE `etudiant`
  ADD CONSTRAINT `SECTION_FK` FOREIGN KEY (`IdSection`) REFERENCES `section` (`IdSection`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
