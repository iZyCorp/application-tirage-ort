﻿using System;
using System.Windows.Forms;

namespace ApplicationTirage {
    internal static class Program {
        /// <summary>
        ///     Main Program Entry
        /// </summary>
        [STAThread]
        private static void Main() {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }
    }
}