namespace ApplicationTirage.Object {
    
    /// <summary>
    /// Class represent a classroom name
    /// </summary>
    public class Section {

        private int _identifiant;
        
        private string _libelle;

        public int Identifiant { get; set; }
        public string Libelle { get; set; }

        public Section(int identifiant, string libelle) {
            this._identifiant = identifiant;
            this._libelle = libelle;
        }
    }
}