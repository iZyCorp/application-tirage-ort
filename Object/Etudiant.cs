namespace ApplicationTirage.Object {
    
    /// <summary>
    /// This class represent an object type of a Student which has
    /// A name
    /// A lastName
    /// And a section link to it
    /// </summary>
    public class Etudiant {
        
        private string _nom;

        private string _prenom;

        private Section _section;

        public string Nom {
            get => _nom;
            set => _nom = value;
        }

        public string Prenom {
            get => _prenom;
            set => _prenom = value;
        }

        public Section Section {
            get => _section;
            set => _section = value;
        }

        public Etudiant(string nom, string prenom, Section section) {
            this._nom = nom;
            this._prenom = prenom;
            this._section = section;
        }
    }
}