using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace ApplicationTirage.Data {
    /// <summary>
    /// This class is a representation of the save file where data such as, recent CSV or databases are stored
    /// </summary>
    [JsonObject(MemberSerialization.OptOut)]
    public class Save {

        private List<String> _recentCsv;

        private List<string> _savedDatabases;
        
        public List<string> RecentCsv { get; set; }
        
        public List<string> SavedDatabases { get; set; }

        public Save() {
            _recentCsv = new List<string>();
            _savedDatabases = new List<string>();
        }
        
        public Save(List<string> recentCsv, List<string> savedDatabases) {
            _recentCsv = recentCsv;
            _savedDatabases = savedDatabases;
        }
    }
}