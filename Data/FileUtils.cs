using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;

namespace ApplicationTirage.Data {
    
    /// <summary>
    /// This class is mainly used to manage file interaction
    /// </summary>
    public class FileUtils {
        
        private static readonly string SaveDir = $"{Environment.GetFolderPath(Environment.SpecialFolder.UserProfile)}\\.Tirage";

        
        /// <summary>
        /// The purpose of this procedure, is serializing data Object in a single file
        /// </summary>
        /// <param name="obj">The Save object (which have been deserialized before)</param>
        /// <param name="path">The path of the file, where write data</param>
        private static void SerializeSaveFile(Save obj, string path) {
            string jsonString = JsonConvert.SerializeObject(obj, Formatting.Indented);
            File.WriteAllText(path, jsonString);

        }
        
        /// <summary>
        /// The purpose of this procedure, is deserializing data Object from Data file
        /// </summary>
        /// <param name="path">The path of the file to deserialize</param>
        /// <returns>A proper Save object</returns>
        private static Save DeserializeSaveFile(string path) {
            JsonSerializerSettings json = new JsonSerializerSettings();
            json.Formatting = Formatting.Indented;
            dynamic obj = JsonConvert.DeserializeObject<Save>(File.ReadAllText(path), json);
            return obj;
        }

        /// <summary>
        /// Procedure to generate the application directory and save file though
        /// </summary>
        public static void GenerateDataFolder() {
            if (!Directory.Exists(SaveDir)) {
                Directory.CreateDirectory(SaveDir);
            }

            if (!File.Exists($"{SaveDir}\\save.json")) {
                File.Create($"{SaveDir}\\save.json").Close();
                SerializeSaveFile(new Save(), $"{SaveDir}\\save.json");
            }
        }

        /// <summary>
        /// Basic function to retrieve recent Csv Paths
        /// </summary>
        /// <returns>A string list of CSV paths</returns>
        public static List<string> GetCsvPaths() {
            Save save = DeserializeSaveFile($"{SaveDir}\\save.json");
            List<string> result = new List<string>();
            if (save.RecentCsv != null) {
                foreach (var path in save.RecentCsv) {
                    result.Add(path);
                }
            }

            return result;
        }

        /// <summary>
        /// Method to return a save file (if exists)
        /// </summary>
        /// <returns>A save object</returns>
        public static Save ReturnSaveFile() {
            return DeserializeSaveFile($"{SaveDir}\\save.json");
        }

        /// <summary>
        /// Procedure to add a Csv path in the save file
        /// </summary>
        /// <param name="path">The path of save directory</param>
        public static void AddRecentCsvToSaveFile(string path) {
            Save save = ReturnSaveFile();
            if (save.RecentCsv != null) {
                if(!save.RecentCsv.Contains(path)) {
                    save.RecentCsv.Add(path);
                }
            } else {
                List<String> recentCsv = new List<string>();
                recentCsv.Add(path);
                save.RecentCsv = recentCsv;
            }
            SerializeSaveFile(save, $"{SaveDir}\\save.json");
        }

        /// <summary>
        /// A simple function to serialize data in save file
        /// </summary>
        /// <param name="save">A proper Save object</param>
        public static void EditSaveFile(Save save) {
            Save currentSave = ReturnSaveFile();
            currentSave.RecentCsv = save.RecentCsv;
            currentSave.SavedDatabases = save.SavedDatabases;
            SerializeSaveFile(currentSave, $"{SaveDir}\\save.json");
        }
    }
}