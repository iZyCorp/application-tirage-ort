using System.ComponentModel;

namespace ApplicationTirage {
    partial class DataBaseForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DataBaseForm));
            this.bConnect = new System.Windows.Forms.Button();
            this.bConnectAndSave = new System.Windows.Forms.Button();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.lPort = new System.Windows.Forms.Label();
            this.tbPort = new System.Windows.Forms.TextBox();
            this.lBdd = new System.Windows.Forms.Label();
            this.lBddName = new System.Windows.Forms.Label();
            this.lPassword = new System.Windows.Forms.Label();
            this.lUserID = new System.Windows.Forms.Label();
            this.lDataSource = new System.Windows.Forms.Label();
            this.tbBdd = new System.Windows.Forms.TextBox();
            this.tbPassword = new System.Windows.Forms.TextBox();
            this.tbUserID = new System.Windows.Forms.TextBox();
            this.tbDataSource = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // bConnect
            // 
            this.bConnect.Anchor = ((System.Windows.Forms.AnchorStyles) ((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.bConnect.Location = new System.Drawing.Point(3, 4);
            this.bConnect.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.bConnect.Name = "bConnect";
            this.bConnect.Size = new System.Drawing.Size(206, 26);
            this.bConnect.TabIndex = 9;
            this.bConnect.Text = "Connexion";
            this.bConnect.UseVisualStyleBackColor = true;
            this.bConnect.Click += new System.EventHandler(this.bConnect_Click);
            // 
            // bConnectAndSave
            // 
            this.bConnectAndSave.Anchor = ((System.Windows.Forms.AnchorStyles) ((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.bConnectAndSave.Location = new System.Drawing.Point(215, 4);
            this.bConnectAndSave.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.bConnectAndSave.Name = "bConnectAndSave";
            this.bConnectAndSave.Size = new System.Drawing.Size(206, 26);
            this.bConnectAndSave.TabIndex = 13;
            this.bConnectAndSave.Text = "Sauv + Conn";
            this.bConnectAndSave.UseVisualStyleBackColor = true;
            this.bConnectAndSave.Click += new System.EventHandler(this.bConnectAndSave_Click);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 5;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.Controls.Add(this.bConnectAndSave, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.bConnect, 1, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(-2, 379);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(424, 34);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // lPort
            // 
            this.lPort.Anchor = ((System.Windows.Forms.AnchorStyles) ((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right)));
            this.lPort.AutoSize = true;
            this.lPort.Location = new System.Drawing.Point(3, 332);
            this.lPort.Name = "lPort";
            this.lPort.Size = new System.Drawing.Size(135, 44);
            this.lPort.TabIndex = 12;
            this.lPort.Text = "Port";
            this.lPort.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbPort
            // 
            this.tbPort.Location = new System.Drawing.Point(144, 335);
            this.tbPort.Name = "tbPort";
            this.tbPort.Size = new System.Drawing.Size(135, 27);
            this.tbPort.TabIndex = 10;
            this.tbPort.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lBdd
            // 
            this.lBdd.Anchor = ((System.Windows.Forms.AnchorStyles) ((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lBdd.AutoSize = true;
            this.lBdd.Location = new System.Drawing.Point(144, 35);
            this.lBdd.Name = "lBdd";
            this.lBdd.Size = new System.Drawing.Size(135, 20);
            this.lBdd.TabIndex = 8;
            this.lBdd.Text = "Connexion BDD";
            this.lBdd.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lBddName
            // 
            this.lBddName.Anchor = ((System.Windows.Forms.AnchorStyles) ((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right)));
            this.lBddName.AutoSize = true;
            this.lBddName.Location = new System.Drawing.Point(3, 260);
            this.lBddName.Name = "lBddName";
            this.lBddName.Size = new System.Drawing.Size(135, 48);
            this.lBddName.TabIndex = 7;
            this.lBddName.Text = "Nom de BDD";
            this.lBddName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lPassword
            // 
            this.lPassword.Anchor = ((System.Windows.Forms.AnchorStyles) ((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right)));
            this.lPassword.AutoSize = true;
            this.lPassword.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lPassword.Location = new System.Drawing.Point(3, 197);
            this.lPassword.Name = "lPassword";
            this.lPassword.Size = new System.Drawing.Size(135, 42);
            this.lPassword.TabIndex = 6;
            this.lPassword.Text = "Mot de passe";
            this.lPassword.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lUserID
            // 
            this.lUserID.Anchor = ((System.Windows.Forms.AnchorStyles) ((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right)));
            this.lUserID.AutoSize = true;
            this.lUserID.Location = new System.Drawing.Point(3, 139);
            this.lUserID.Name = "lUserID";
            this.lUserID.Size = new System.Drawing.Size(135, 39);
            this.lUserID.TabIndex = 5;
            this.lUserID.Text = "UserID";
            this.lUserID.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lDataSource
            // 
            this.lDataSource.Anchor = ((System.Windows.Forms.AnchorStyles) ((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right)));
            this.lDataSource.AutoSize = true;
            this.lDataSource.Location = new System.Drawing.Point(3, 79);
            this.lDataSource.Name = "lDataSource";
            this.lDataSource.Size = new System.Drawing.Size(135, 43);
            this.lDataSource.TabIndex = 4;
            this.lDataSource.Text = "DataSource";
            this.lDataSource.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbBdd
            // 
            this.tbBdd.Anchor = ((System.Windows.Forms.AnchorStyles) ((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tbBdd.Location = new System.Drawing.Point(144, 270);
            this.tbBdd.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbBdd.Name = "tbBdd";
            this.tbBdd.Size = new System.Drawing.Size(135, 27);
            this.tbBdd.TabIndex = 3;
            this.tbBdd.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbPassword
            // 
            this.tbPassword.Anchor = ((System.Windows.Forms.AnchorStyles) ((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tbPassword.Location = new System.Drawing.Point(144, 204);
            this.tbPassword.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbPassword.Name = "tbPassword";
            this.tbPassword.Size = new System.Drawing.Size(135, 27);
            this.tbPassword.TabIndex = 2;
            this.tbPassword.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbUserID
            // 
            this.tbUserID.Anchor = ((System.Windows.Forms.AnchorStyles) ((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tbUserID.Location = new System.Drawing.Point(144, 145);
            this.tbUserID.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbUserID.Name = "tbUserID";
            this.tbUserID.Size = new System.Drawing.Size(135, 27);
            this.tbUserID.TabIndex = 1;
            this.tbUserID.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbDataSource
            // 
            this.tbDataSource.Anchor = ((System.Windows.Forms.AnchorStyles) ((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDataSource.Location = new System.Drawing.Point(144, 87);
            this.tbDataSource.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbDataSource.Name = "tbDataSource";
            this.tbDataSource.Size = new System.Drawing.Size(135, 27);
            this.tbDataSource.TabIndex = 0;
            this.tbDataSource.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.Controls.Add(this.tbDataSource, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.tbUserID, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.tbPassword, 1, 7);
            this.tableLayoutPanel1.Controls.Add(this.tbBdd, 1, 9);
            this.tableLayoutPanel1.Controls.Add(this.lDataSource, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.lUserID, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.lPassword, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.lBddName, 0, 9);
            this.tableLayoutPanel1.Controls.Add(this.lBdd, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.tbPort, 1, 11);
            this.tableLayoutPanel1.Controls.Add(this.lPort, 0, 11);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(-2, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 12;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 31.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 68.33334F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 17F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 39F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 19F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 42F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 21F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 48F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 43F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(424, 376);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // DataBaseForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(421, 414);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Font = new System.Drawing.Font("Consolas", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon) (resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "DataBaseForm";
            this.Text = "Connexion à une BDD";
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);
        }

        #endregion
        private System.Windows.Forms.Label bddL;
        private System.Windows.Forms.Button bConnect;
        private System.Windows.Forms.Button bConnectAndSave;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label lPort;
        private System.Windows.Forms.TextBox tbPort;
        private System.Windows.Forms.Label lBdd;
        private System.Windows.Forms.Label lBddName;
        private System.Windows.Forms.Label lPassword;
        private System.Windows.Forms.Label lUserID;
        private System.Windows.Forms.Label lDataSource;
        private System.Windows.Forms.TextBox tbBdd;
        private System.Windows.Forms.TextBox tbPassword;
        private System.Windows.Forms.TextBox tbUserID;
        private System.Windows.Forms.TextBox tbDataSource;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
    }
}