﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using ApplicationTirage.Data;
using ApplicationTirage.Object;
using MySql.Data.MySqlClient;

namespace ApplicationTirage {
    
    /// <summary>
    /// This class is the main Form of the application.
    /// It is there where we are loading and manipulating our students
    /// </summary>
    public partial class MainForm : Form {

        // Cache
        private readonly IDictionary<string, List<Etudiant>> _storedStudents = new Dictionary<string, List<Etudiant>>();
        private readonly List<Etudiant> _selectedStudents = new List<Etudiant>();
        private List<Etudiant> _waitingStudents = new List<Etudiant>();

        /// <summary>
        /// Main Constructor of our app, initializing, Cache, graphics and Menubar here
        /// </summary>
        public MainForm() {
            InitializeComponent();

            FileUtils.GenerateDataFolder();
            
            unselectedStudentTable.EnableHeadersVisualStyles = false;
            selectedStudentTable.EnableHeadersVisualStyles = false;

            // Initializing Main menu app
            MainMenu _mainMenu = new MainMenu();
            
            var fileMenu = _mainMenu.MenuItems.Add("&Fichier");
            fileMenu.MenuItems.Add(new MenuItem("&Importer un CSV", Import_Click));
            MenuItem openRecent = new MenuItem("&Ouvrir un CSV récent");

            foreach (string paths in FileUtils.GetCsvPaths()) {
                openRecent.MenuItems.Add(new MenuItem(paths, OpenRecentCSV_Click));
            }
            fileMenu.MenuItems.Add(openRecent);
            fileMenu.MenuItems.Add("-");
            fileMenu.MenuItems.Add(new MenuItem("Connexion à une BDD", MIConnectBdd_Click));
            fileMenu.MenuItems.Add("-");
            fileMenu.MenuItems.Add(new MenuItem("&Quitter", Exit_Click));
            Menu = _mainMenu;
            
            var startMenu = _mainMenu.MenuItems.Add("&Démarrer");
            startMenu.MenuItems.Add(new MenuItem("&Tirage", Tirage_Click));
            startMenu.MenuItems.Add("-");
            startMenu.MenuItems.Add(new MenuItem("&Nettoyer tables", CleanTable_Click));

            var optionMenu = _mainMenu.MenuItems.Add("Options");
            optionMenu.MenuItems.Add(new MenuItem("&Thèmes"));
            optionMenu.MenuItems.Add(new MenuItem("Montrer la console"));
            Menu = _mainMenu;
            
        }

        #region events

        /// <summary>
        /// Event used while trying to import a CSV from File menu
        /// </summary>
        /// <param name="sender">This is the object concerned by the event</param>
        /// <param name="e">The event targeted</param>
        private void Import_Click(object sender, EventArgs e) {
            
            CleanApplication();
            
            var explorer = new OpenFileDialog();
            explorer.Title = "Selectionnez un fichier CSV";
            explorer.DefaultExt = ".csv";
            explorer.Filter = "CSV Files (L*.csv)|L*.csv";
            explorer.CheckFileExists = true;
            explorer.CheckPathExists = true;
            if (explorer.ShowDialog() == DialogResult.OK) {
                FileUtils.AddRecentCsvToSaveFile(explorer.FileName);
                
                ImportCsv(explorer.FileName);
                UpdateDisplay();
            }
        }
        
        /// <summary>
        /// Event called while trying to import a Recent CSV file
        /// </summary>
        /// <param name="sender">This is the object concerned by the event</param>
        /// <param name="e">The event targeted</param>
        private void OpenRecentCSV_Click(object sender, EventArgs e) {
            if (sender is MenuItem) {
                MenuItem menuItem = (MenuItem) sender;
                string path = menuItem.Text;

                CleanApplication();
                
                try {
                    ImportCsv(path);
                }
                catch (Exception exception) {
                    MessageBox.Show("Impossible d'importer le CSV", "Erreur");
                }
                
                UpdateDisplay();
            }
        }

        /// <summary>
        /// Event called while trying to click on the exit Button in File menu
        /// </summary>
        /// <param name="sender">This is the object concerned by the event</param>
        /// <param name="e">The event targeted</param>
        private void Exit_Click(object sender, EventArgs e) {
            Application.Exit();
        }

        /// <summary>
        /// Event called while trying to pick a random student from loaded cache
        /// </summary>
        /// <param name="sender">This is the object concerned by the event</param>
        /// <param name="e">The event targeted</param>
        private void Tirage_Click(object sender, EventArgs e) {
            if (_waitingStudents.Count != 0) {
                var index = new Random().Next(0, _waitingStudents.Count);
                var selectedStudent = _waitingStudents[index];
                _selectedStudents.Add(selectedStudent);
                _waitingStudents.Remove(selectedStudent);
                UpdateDisplay();
                MessageBox.Show($"{selectedStudent.Nom} {selectedStudent.Prenom}", "Tirage");
            }
            else
                MessageBox.Show("Il n'y a plus d'étudiant à tirer! Nettoyez les tables ou importez un nouveau csv.",
                    "Fin de tirage");
        }
        
        /// <summary>
        /// Event called while clicking on "Connect to database" in file menu
        /// </summary>
        /// <param name="sender">This is the object concerned by the event</param>
        /// <param name="e">The event targeted</param>
        private void MIConnectBdd_Click(object sender, EventArgs e) {
            DataBaseForm dataBaseForm = new DataBaseForm();
            dataBaseForm.ShowDialog(this);
            dataBaseForm.TopMost = true;
        }

        /// <summary>
        /// Event called while trying to click on "Clean table" in menubar
        /// </summary>
        /// <param name="sender">This is the object concerned by the event</param>
        /// <param name="e">The event targeted</param>
        private void CleanTable_Click(object sender, EventArgs e) {
            CleanApplication();
        }

        #endregion


        #region methods

        /// <summary>
        /// Simple procedure to Update the content of our WindowForm objects
        /// </summary>
        private void UpdateDisplay() {
            unselectedStudentTable.Rows.Clear();
            unselectedStudentTable.RowCount = _waitingStudents.Count == 0 ? 1 : _waitingStudents.Count;

            foreach (var student in _waitingStudents) {
                unselectedStudentTable.Rows[_waitingStudents.IndexOf(student)].Cells[0].Value = student.Nom;
                unselectedStudentTable.Rows[_waitingStudents.IndexOf(student)].Cells[1].Value = student.Prenom;
            }

            foreach (var student in _selectedStudents) {
                selectedStudentTable.Rows[_selectedStudents.IndexOf(student)].Cells[0].Value = student.Nom;
                selectedStudentTable.Rows[_selectedStudents.IndexOf(student)].Cells[1].Value = student.Prenom;
            }
        }

        /// <summary>
        /// Simple method to import a CSV
        /// This is usually used for Recent CSV import and classic import
        /// </summary>
        /// <param name="fileName"></param>
        private void ImportCsv(string fileName) {
            var fs = new FileStream(fileName, FileMode.OpenOrCreate);
            var sr = new StreamReader(fs);

            var line = "";
            while ((line = sr.ReadLine()) != null) {
                var name = line.Split(';')[0];
                var surname = line.Split(';')[1];
                _waitingStudents.Add(new Etudiant(name, surname, new Section(0, "none")));
            }

            sr.Close();
            fs.Close();
            
            selectedStudentTable.RowCount = _waitingStudents.Count;
        }

        /// <summary>
        /// Procedure that manage the database connection and database data fetcher
        /// </summary>
        /// <param name="builder">The SQL connection string</param>
        public void ConnectToDatabase(string builder) {
            using (MySqlConnection connection = new MySqlConnection(builder)) {
                try {
                    
                    /*
                     * HERE WE ARE TRYING TO CONNECT TO OUR DATABASE
                     * AND FROM THERE, IMPORT OUR STUDENTS
                     */
                    connection.Open();
                    
                    // Storing database in save file
                    Save newSave = FileUtils.ReturnSaveFile();
                    if (newSave.SavedDatabases != null) newSave.SavedDatabases.Add(builder);
                    else newSave.SavedDatabases = new List<string>() {builder};
                    FileUtils.EditSaveFile(newSave);
                    
                    // Cleaning the app cache and tables
                    CleanApplication();

                    /* == Making our request
                     * |-------------
                     * | 0 = Nom
                     * | 1 = Prenom
                     * | 2 = Section
                     * |-------------
                     * | 3 = libelle
                     * |______________
                     */
                    MySqlCommand sqlCmd = new MySqlCommand("SELECT etudiant.NomEtudiant, etudiant.PrenomEtudiant, etudiant.IdSection, section.libelle FROM etudiant INNER JOIN section on etudiant.IdSection = section.IdSection;", connection);

                    //Initializing the reader
                    MySqlDataReader reader = sqlCmd.ExecuteReader();

                    // Reading the data
                    while (reader.Read()) {
                        
                        // Here we are recovering the data
                        string nom = reader.GetValue(0).ToString();
                        string prenom = reader.GetValue(1).ToString();
                        string libelle = reader.GetValue(3).ToString();
                        Section sectionId = new Section(reader.GetInt32(2), libelle);

                        if (!cbSection.Items.Contains(libelle)) cbSection.Items.Add(libelle);
                            
                        List<Etudiant> students;

                        // Checking if the section is stored and create it if not
                        if (!_storedStudents.ContainsKey(libelle)) {
                            students = new List<Etudiant>();
                            students.Add(new Etudiant(nom, prenom, sectionId));
                            _storedStudents.Add(libelle, students);
                        } else {
                            // Adding the studient in the studient list contained by our dictionnary
                            students = _storedStudents[libelle];
                            students.Add(new Etudiant(nom, prenom, sectionId));
                        }
                    }

                    reader.Close();

                } catch (Exception exception) {
                    MessageBox.Show(exception.StackTrace, exception.Message);
                }
                
                finally {
                    
                    /*
                     * IF BY CHANCE SOMETHING WENT WRONG
                     * WE CLOSE IN THE FINALLY BLOCK THE DATABASE CONNECTION
                     */
                    connection.Close();
                }
            }
        }

        /// <summary>
        /// Simple method to clean the entire application cache.
        /// Save file aren't include, obviously
        /// </summary>
        private void CleanApplication() {
            unselectedStudentTable.Rows.Clear();
            selectedStudentTable.Rows.Clear();
            _waitingStudents.Clear();
            _selectedStudents.Clear();
        }

        #endregion

        /// <summary>
        /// This event is for the combobox that manage the sections.
        /// </summary>
        /// <param name="sender">This is the object concerned by the event</param>
        /// <param name="e">The event targeted</param>
        private void cbSection_SelectedIndexChanged(object sender, EventArgs e) {
            string section = this.cbSection.SelectedItem.ToString();
            CleanApplication();
            this._waitingStudents = new List<Etudiant>(_storedStudents[section]);
            selectedStudentTable.RowCount = _waitingStudents.Count;
            UpdateDisplay();
        }
    }
}