using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using ApplicationTirage.Data;

namespace ApplicationTirage {
    
    public partial class DataBaseForm : Form {

        /// <summary>
        /// Main Constructor of that form, used to load component and especially fetching if data exists,
        /// a possible connection string to fill the blank
        /// </summary>
        public DataBaseForm() {
            InitializeComponent();
            tbPassword.PasswordChar = '*';
            if (FileUtils.ReturnSaveFile().SavedDatabases != null) {
                string sqlConnection = FileUtils.ReturnSaveFile().SavedDatabases[0];
                string dataSource = sqlConnection.Split(';')[0].Replace("Data Source=", "");
                string dataBase = sqlConnection.Split(';')[1].Replace("Initial Catalog=", "");
                string userId = sqlConnection.Split(';')[2].Replace("User ID=", "");
                string password = sqlConnection.Split(';')[3].Replace("Password=", "");

                tbDataSource.Text = dataSource.Split(',')[0];
                tbBdd.Text = dataBase;
                tbUserID.Text = userId;
                tbPassword.Text = password;
                tbPort.Text = Regex.Match(dataSource, @"\d+").Value;
            }

        }

        /// <summary>
        /// Event called while trying to click on "Connect" button
        /// </summary>
        /// <param name="sender">This is the object concerned by the event</param>
        /// <param name="e">The event targeted</param>
        private void bConnect_Click(object sender, EventArgs e) {
            ((MainForm) Owner).ConnectToDatabase(BuildSqlConnection().ConnectionString);
            Close();
        }

        /// <summary>
        /// Event called while trying to click on "Save & Connect" button
        /// </summary>
        /// <param name="sender">This is the object concerned by the event</param>
        /// <param name="e">The event targeted</param>
        private void bConnectAndSave_Click(object sender, EventArgs e) {
            List<string> savedDatabases = FileUtils.ReturnSaveFile().SavedDatabases == null
                ? new List<string>()
                : FileUtils.ReturnSaveFile().SavedDatabases;
            savedDatabases.Add(BuildSqlConnection().ConnectionString);
            FileUtils.EditSaveFile(new Save(FileUtils.ReturnSaveFile().RecentCsv, savedDatabases));
            
            ((MainForm) Owner).ConnectToDatabase(BuildSqlConnection().ConnectionString);
            Close();
        }

        /// <summary>
        /// This function return a proper sql string connection
        /// </summary>
        /// <returns>Sql string connection</returns>
        private SqlConnectionStringBuilder BuildSqlConnection() {
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            builder.DataSource = tbPort.Text.Length != 0 ? $"{tbDataSource.Text},{tbPort.Text}" : $"{tbDataSource.Text}";
            builder.UserID = $"{tbUserID.Text}";            
            builder.Password = $"{tbPassword.Text}";
            builder.InitialCatalog = $"{tbBdd.Text}";
            return builder;
        }
    }
}